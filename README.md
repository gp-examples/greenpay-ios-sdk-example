# **GreenPay iOS SDK Example**

GreenPay is a payments platform that allows wesites, desktop and mobile apps to process payments.

In order to process payments you must create an account on [GreenPay](https://www.greenpay.me/).

## Installation

The iOS SDK is published as a [Cocoapod](https://cocoapods.org/). To install, simply add this line to your pod file:

```bash
pod 'GreenPay'
```

## Usage

First, you need to create a GreenPay ```GreenPayConfig``` object to configure the SDK with your account information. This information will be provided by the GreenPay Team when you create your GreenPay Account.

Second, call the initGreenPaySDK method to initialize the SDK.

```swift
import GreenpaySDK
...
let greenPaySecret = "";
let greenPayMerchantId = "";
let greenPayTerminal = "";
let greenPayPublicKey = "";
...
let config: GreenpayConfig = GreenpayConfig(greenPaySecret: greenPaySecret, greenPayMerchantId: greenPayMerchantId, greenPayTerminal: greenPayTerminal, greenPayPublickKey: greenPayPublicKey);

Greenpay.shared.initGreenpaySDK(config: config);
Greenpay.shared.enableLogs(enableLogs: true);
```
Additionally, you can switch between Sandbox and Production environments with this call (By default the SDK uses the Production environment).

```swift
Greenpay.shared.enableSandboxMode(sandboxModeEnabled: true);
```
## Making a Payment

First, a order has to be created from your backend to Greenpay. This will generate a Session Id and a Transaction Token. You need these values in order to make a payment. You will need to create a ```GreenPayOrderResult``` object to hold these values.

Second, from your app you can call ```checkoutExistingOrder```  with the Credit Card to process and the Session Id and Transaction Token obtained from your backend (the amount was sent when creating the order from your backend).

The returning object  ```GreenPayCheckoutOrderResult``` will have the transaction confirmation from GreenPay.

```swift
let gpCreditCard:GreenpayCreditCard = self.getCardData();
let gpOrderInfo:GreenpayOrderInfo = self.getOrderInfo();
Greenpay.shared.createNewGreenpayOrder(greenPayOrderInfo: gpOrderInfo) { (greenPayOrderResult, success, error) in
    if(success) {
        let message: String = "createNewGreenPayOrder Success: Session: " + greenPayOrderResult!.securityInfo.session + " , Token: " + greenPayOrderResult!.securityInfo.token;
        print(message);
        Greenpay.shared.checkoutExistingOrder(greenPayCreditCard: gpCreditCard, greenPayOrderResult: greenPayOrderResult!) { (greenPayCheckoutOrderResult, success, error) in
            if (success) {
                let message: String = "Order paid. Status:" + String(greenPayCheckoutOrderResult!.status) + ", Auth: " + String(greenPayCheckoutOrderResult!.authorization);
                self.alert(message: message);
            } else {
                let message: String = "Error making the payment: " + error!.localizedDescription
                self.alert(message: message);
            }
        }
    } else {
        let message: String = "Error creating the order for making the payment: " + error!.localizedDescription
        self.alert(message: message);
    }
}

func alert(message: String) {
    DispatchQueue.main.async {
        let alertViewController: UIAlertController = UIAlertController(title: "Greenpay", message: message, preferredStyle: .alert);
        alertViewController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil));
        self.present(alertViewController, animated:true, completion: nil);
    }
}

func getCardData() -> GreenpayCreditCard {
    let cardHolder:String = "Jhon Doe";
    let cardNumber = "4795736054664396";
    let cvc = "123";
    let expirationMonth = 9;
    let expirationYear = 21;
    let nickname = "visa2449";
    return GreenpayCreditCard(cardHolder: cardHolder, cardNumber:cardNumber, expirationMonth:expirationMonth, expirationYear:expirationYear, cvc: cvc, nickname:nickname);
}

func getOrderInfo() -> GreenpayOrderInfo {
    let customerName = "Jhon Doe"
    let customerId = "114910122"
    let amount:Double = 100
    let currency = "CRC"
    let pdescription = "Bus ticket - 0001"
    let orderReference = "orderReference-" + String(Int.random(in: 0...100));
    let product: GreenpayProduct = GreenpayProduct(description: "ticket electronico", skuId: "001", quantity: 1, price: 100, type: "Digital")
    var products : [GreenpayProduct] = [GreenpayProduct]();
    products.append(product);
    let address:GreenpayAddress = GreenpayAddress (country: "CR", street1: "Cartago")
    let customerEmail="jhon@mail.com";
    let gpInfo = GreenpayOrderInfo(customerName: customerName, customerEmail: customerEmail, customerId: customerId,shippingAddress: address, billingAddress: address, amount: amount, currency: currency, description: pdescription, orderReference: orderReference, products: products)
    return gpInfo
}
```
## Tokenizing a Card 

First, a order has to be created from your backend to GreenPay. This will generate a Session Id and a Transaction Token. You need these values in order to tokenize a card. You will need to create a ```GreenPayOrderResult``` object to hold these values.

Second, from your app you can call ```checkoutExistingTokenizeCardOrder```  with the Credit Card to tokenize and the Session Id and Transaction Token obtained from your backend (the amount was sent when creating the order from your backend).

The returning object  ```GreenPayTokenizeCardCheckoutOrderResult``` will have the Card Token representing the Card in GreenPay.

```swift
var gpTokenizedCreditCard : GreenpayTokenizedCard? = nil;
...
let gpCreditCard:GreenpayCreditCard = self.getCardData();
let greenPayTokenizeCardOrderInfo: GreenpayTokenizeCardOrderInfo = GreenpayTokenizeCardOrderInfo(resquestId: GreenpayTokenizeCardOrderInfo.generateRandomRequestId());

Greenpay.shared.createNewTokenizeCardGreenpayOrder(greenPayTokenizeCardOrderInfo: greenPayTokenizeCardOrderInfo) { (greenPayOrderResult, success, error) in
    if (success) {
        Greenpay.shared.checkoutExistingTokenizeCardOrder(greenPayCreditCard: gpCreditCard, greenPayOrderResult: greenPayOrderResult!) { (greenPayTokenizeCardCheckoutOrderResult, sucess, error) in
            if (success) {
                self.gpTokenizedCreditCard=GreenpayTokenizedCard( token: greenPayTokenizeCardCheckoutOrderResult!.token);
                let message: String = "Checkout Tokenize Card Order Response. Token: " + greenPayTokenizeCardCheckoutOrderResult!.token;
                self.alert(message: message);
            } else {
                let message: String = "Error tokenizing the card: " + error!.localizedDescription;
                self.alert(message: message);
            }
        }
    } else {
        let message: String = "Error making the order for tokenizing the card: " + error!.localizedDescription;
        self.alert(message: message);
    }
}

func alert(message: String) {
    DispatchQueue.main.async {
        let alertViewController: UIAlertController = UIAlertController(title: "Greenpay", message: message, preferredStyle: .alert);
        alertViewController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil));
        self.present(alertViewController, animated:true, completion: nil);
    }
}

func getCardData() -> GreenpayCreditCard {
    let cardHolder:String = "Jhon Doe";
    let cardNumber = "4795736054664396";
    let cvc = "123";
    let expirationMonth = 9;
    let expirationYear = 21;
    let nickname = "visa2449";
    return GreenpayCreditCard(cardHolder: cardHolder, cardNumber:cardNumber, expirationMonth:expirationMonth, expirationYear:expirationYear, cvc: cvc, nickname:nickname);
}
```
## Making a Payment with a Tokenized Card

First, a order has to be created from your backend to GreenPay. This will generate a Session Id and a Transaction Token. You need these values in order to make a payment. You will need to create a ```GreenPayOrderResult``` object to hold these values.

Second, from your app you can call ```checkoutExistingOrder```  with the Tokenized Card to process and the Session Id and Transaction Token obtained from your backend (the amount was sent when creating the order from your backend).

The returning object  ```GreenPayCheckoutOrderResult``` will have the transaction confirmation from GreenPay.

```swift
let gpOrderInfo:GreenpayOrderInfo = self.getOrderInfo();
Greenpay.shared.createNewGreenpayOrder(greenPayOrderInfo: gpOrderInfo) { (greenPayOrderResult, success, error) in
    if (success) {
        Greenpay.shared.checkoutExistingOrder(greenPayTokenizedCard: self.gpTokenizedCreditCard!, greenPayOrderResult: greenPayOrderResult!) { (greenPayCheckoutOrderResult, success, error) in
            if (success) {
                let message: String = "Payment made successfully with Tokenized Card: Authorization: " + greenPayCheckoutOrderResult!.authorization;
                self.alert(message: message);
            } else {
                let message: String = "Error making payment with Tokenized Card" + error!.localizedDescription;
                self.alert(message: message);
            }
        }
    } else {
        let message: String = "Error creating order for making payment with Tokenized Card" + error!.localizedDescription;
        self.alert(message: message);
    }
}

func alert(message: String) {
    DispatchQueue.main.async {
        let alertViewController: UIAlertController = UIAlertController(title: "Greenpay", message: message, preferredStyle: .alert);
        alertViewController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil));
        self.present(alertViewController, animated:true, completion: nil);
    }
}

func getOrderInfo() -> GreenpayOrderInfo {
    let customerName = "Jhon Doe"
    let customerId = "114910122"
    let amount:Double = 100
    let currency = "CRC"
    let pdescription = "Bus ticket - 0001"
    let orderReference = "orderReference-" + String(Int.random(in: 0...100));
    let product: GreenpayProduct = GreenpayProduct(description: "ticket electronico", skuId: "001", quantity: 1, price: 100, type: "Digital")
    var products : [GreenpayProduct] = [GreenpayProduct]();
    products.append(product);
    let address:GreenpayAddress = GreenpayAddress (country: "CR", street1: "Cartago")
    let customerEmail="jhon@mail.com";
    let gpInfo = GreenpayOrderInfo(customerName: customerName, customerEmail: customerEmail, customerId: customerId,shippingAddress: address, billingAddress: address, amount: amount, currency: currency, description: pdescription, orderReference: orderReference, products: products)
    return gpInfo
}
```
## Collecting data for Kount service with a provided session id

In order to comply with the Greenpay service regulation, in case you have processed a checkout in the backend and you have to collect data in Kount using the provided session id. 

```swift
Greenpay.shared.collectForSession(sessionID: providedSessionID).done({ (sessionID) in
    print("Success collecting info for Kount service");
}).catch({ (error) in
    print("ERROR collection info for Kount service");
});
```
