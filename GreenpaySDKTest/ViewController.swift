//
//  ViewController.swift
//  GreenpaySDKTest
//
//  Created by Jesús Quirós on 8/3/20.
//  Copyright © 2020 Jesús Quirós. All rights reserved.
//

import UIKit
import GreenpaySDK

class ViewController: UIViewController {
    
    @IBOutlet weak var pagarConTokenButton: UIButton!
    
    var gpTokenizedCreditCard : GreenpayTokenizedCard? = nil;
    
    //your secret provided by Greenpay
    private static let greenPaySecret = "Njg3RDM5RDc1QkE3MzI0MTJEM0YwMjAxRjdFRDQ4NUI2RUQ2NEU2RDQ3NkVCMDJEMTNEQzEyQTUyMkY2RURBMjkxODY2OTFDRDBBMTM1QTE4OTdCNzkzNzhBREQ3QkU5RTMxMUFGODNEMzQ0NzY3MjAzQjVDQjNEQjBDRDJGQzk=";
    //your merchant id provided by Greenpay
    private static let greenPayMerchantId = "06c229a2-2632-46ac-a9a5-f63397e59573";
    //terminal provided by Greenpay
    private static let greenPayTerminal = "Postman Examples-efe6-BNCR-CRC";
    //public key provided by Greenpay.
    private static let greenPayPublicKey = "-----BEGIN PUBLIC KEY-----\n" +
        "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCWPaHuE0vRonynIwVezCc3+Mc7\n" +
        "89j2iSnH56pyPN8zkWpPPMGCPcnU1Qj1dXH67YMO9ZXfCpyKKNURxks31MAXc9K1\n" +
        "QuA1+limyFe4emtO89CGGMb4iMU/Dl0wM+3Q5I/EVzlQE15tPbsAEqLtnNuPvOtv\n" +
        "G1xBvtciPCXpIdmAPwIDAQAB\n" +
        "-----END PUBLIC KEY-----";

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.pagarConTokenButton.isEnabled = false;
        
        let config: GreenpayConfig = GreenpayConfig(greenPaySecret: ViewController.greenPaySecret, greenPayMerchantId: ViewController.greenPayMerchantId, greenPayTerminal: ViewController.greenPayTerminal, greenPayPublickKey: ViewController.greenPayPublicKey);
               
               Greenpay.shared.initGreenpaySDK(config: config);
               Greenpay.shared.enableSandboxMode(sandboxModeEnabled: true);
               Greenpay.shared.enableLogs(enableLogs: true);
    }

    @IBAction func pagar(_ sender: Any) {
        let gpCreditCard:GreenpayCreditCard = self.getCardData();
        let gpOrderInfo:GreenpayOrderInfo = self.getOrderInfo();
        Greenpay.shared.createNewGreenpayOrder(greenPayOrderInfo: gpOrderInfo) { (greenPayOrderResult, success, error) in
            if(success) {
                let message: String = "createNewGreenPayOrder Success: Session: " + greenPayOrderResult!.securityInfo.session + " , Token: " + greenPayOrderResult!.securityInfo.token;
                print(message);
                Greenpay.shared.checkoutExistingOrder(greenPayCreditCard: gpCreditCard, greenPayOrderResult: greenPayOrderResult!) { (greenPayCheckoutOrderResult, success, error) in
                    if (success) {
                        let message: String = "Order paid. Status:" + String(greenPayCheckoutOrderResult!.status) + ", Auth: " + String(greenPayCheckoutOrderResult!.authorization);
                        self.alert(message: message);
                    } else {
                        let message: String = "\(error!.errorCode) Error making the payment: " + error!.localizedDescription
                        self.alert(message: message);
                    }
                }
            } else {
                let message: String = "\(error!.errorCode) Error creating the order for making the payment: " + error!.localizedDescription
                self.alert(message: message);
            }
        }
    }
    
    @IBAction func tokenizar(_ sender: Any) {
        let gpCreditCard:GreenpayCreditCard = self.getCardData();
        let greenPayTokenizeCardOrderInfo: GreenpayTokenizeCardOrderInfo = GreenpayTokenizeCardOrderInfo(resquestId: GreenpayTokenizeCardOrderInfo.generateRandomRequestId());
        
        Greenpay.shared.createNewTokenizeCardGreenpayOrder(greenPayTokenizeCardOrderInfo: greenPayTokenizeCardOrderInfo) { (greenPayOrderResult, success, error) in
            if (success) {
                Greenpay.shared.checkoutExistingTokenizeCardOrder(greenPayCreditCard: gpCreditCard, greenPayOrderResult: greenPayOrderResult!) { (greenPayTokenizeCardCheckoutOrderResult, sucess, error) in
                    if (success) {
                        self.gpTokenizedCreditCard=GreenpayTokenizedCard( token: greenPayTokenizeCardCheckoutOrderResult!.token);
                        DispatchQueue.main.async {
                             self.pagarConTokenButton.isEnabled = true;
                        }
                        let message: String = "Checkout Tokenize Card Order Response. Token: " + greenPayTokenizeCardCheckoutOrderResult!.token;
                        self.alert(message: message);
                    } else {
                        let message: String = "\(error!.errorCode) Error tokenizing the card: " + error!.localizedDescription;
                        self.alert(message: message);
                    }
                }
            } else {
                let message: String = "\(error!.errorCode) Error making the order for tokenizing the card: " + error!.localizedDescription;
                self.alert(message: message);
            }
        }
    }
    
    @IBAction func pagarConToken(_ sender: Any) {
        let gpOrderInfo:GreenpayOrderInfo = self.getOrderInfo();
        Greenpay.shared.createNewGreenpayOrder(greenPayOrderInfo: gpOrderInfo) { (greenPayOrderResult, success, error) in
            if (success) {
                Greenpay.shared.checkoutExistingOrder(greenPayTokenizedCard: self.gpTokenizedCreditCard!, greenPayOrderResult: greenPayOrderResult!) { (greenPayCheckoutOrderResult, success, error) in
                    if (success) {
                        let message: String = "Payment made successfully with Tokenized Card: Authorization: " + greenPayCheckoutOrderResult!.authorization;
                        self.alert(message: message);
                    } else {
                        let message: String = "\(error!.errorCode) Error making payment with Tokenized Card: " + error!.localizedDescription;
                        self.alert(message: message);
                    }
                }
            } else {
                let message: String = "\(error!.errorCode) Error creating order for making payment with Tokenized Card: " + error!.localizedDescription;
                self.alert(message: message);
            }
        }
    }
    
    func alert(message: String) {
        DispatchQueue.main.async {
            let alertViewController: UIAlertController = UIAlertController(title: "Greenpay", message: message, preferredStyle: .alert)
            alertViewController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alertViewController, animated:true, completion: nil);
        }
    }

    func getCardData() -> GreenpayCreditCard {
        let cardHolder:String = "Jhon Doe";
        //success cards
        let cardNumber = "4111111111111111";
//        let cardNumber = "4242424242424242";
        
        //error cards
//        let cardNumber = "4485970366452449";
//        let cardNumber = "4916402417466144";
//        let cardNumber = "4929639391534299";
//        let cardNumber = "4094394314664828";
//        let cardNumber = "4539610492212483";
//        let cardNumber = "4556710706281072";
        let cvc = "123";
        let expirationMonth = 9;
        let expirationYear = 21;
        let nickname = "visa2449";
        return GreenpayCreditCard(cardHolder: cardHolder, cardNumber:cardNumber, expirationMonth:expirationMonth, expirationYear:expirationYear, cvc: cvc, nickname:nickname);
    }

    func getOrderInfo() -> GreenpayOrderInfo {
        let customerName = "Jhon Doe"
        let customerId = "114910122"
        let amount:Double = 100
        let currency = "CRC"
        let pdescription = "Bus ticket - 0001"
        let orderReference = "orderReference-" + String(Int.random(in: 0...100));
        let product: GreenpayProduct = GreenpayProduct(description: "ticket electronico", skuId: "001", quantity: 1, price: 100, type: "Digital")
        var products : [GreenpayProduct] = [GreenpayProduct]();
        products.append(product);
        let address:GreenpayAddress = GreenpayAddress (country: "CR", street1: "Cartago")
        let customerEmail="jhon@mail.com";
        let gpInfo = GreenpayOrderInfo(customerName: customerName, customerEmail: customerEmail, customerId: customerId,shippingAddress: address, billingAddress: address, amount: amount, currency: currency, description: pdescription, orderReference: orderReference, products: products)
        return gpInfo
    }
    
}

